/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "src/PixelClusterizationAlg.h"
#include "src/StripClusterizationAlg.h"

DECLARE_COMPONENT(ActsTrk::PixelClusterizationAlg)
DECLARE_COMPONENT(ActsTrk::StripClusterizationAlg)
