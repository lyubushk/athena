/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "MuonGeoModelTest/MuonGMCheck.h"
#include "MuonGeoModelTest/MuonGMTestOnPrd.h"
#include "MuonGeoModelTest/MuonHitRelocation.h"
#include "../NSWGeoPlottingAlg.h"


DECLARE_COMPONENT(MuonGMCheck)
DECLARE_COMPONENT(MuonGMTestOnPrd)
DECLARE_COMPONENT(MuonHitRelocation)
DECLARE_COMPONENT(NSWGeoPlottingAlg)
